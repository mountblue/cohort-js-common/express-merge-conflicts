const fs = require("fs");
const express = require("express");

const app = express();

app.get("/users", (req, res) => {
    fs.readFile("./data/users.json", "utf-8", (err, data) => {
        if(err) {
            res.status(400)
            .json({
                error: "Internal Server Error",
            })
            .end();
        } else {
            res.status(200)
            .json(data)
            .end();
        }
    });
});

app.get("/todos", (req, res) => {
    fs.readFile("./data/todos.json", "utf-8", (err, data) => {
        if(err) {
            res.status(400)
            .json({
                error: "Internal Server Error",
            })
            .end();
        } else {
            res.status(200)
            .json(data);
        }
    });
});

app.listen(3000, () => {
    console.log(`Listening on port 3000`);
});